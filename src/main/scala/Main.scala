import zio._
import zio.console.putStrLn

object Main extends App {

  type MainEnv =
    Has[ServiceAInterface]
  //Has[ServiceCInterface] with Has[ServiceBInterface] with Has[ServiceAInterface]

  private val layers: ZLayer[Any, Any, MainEnv] = {
    val c_ = ServiceC.live
    val b_ = c_ >>> ServiceB.live
    val a_ = (c_ ++ b_) >>> ServiceA.live

    // c_ ++ b_ ++ a_

    // In this case ServiceA is the ONLY layer exposed to 'program' but
    // it's build vertically, b_ and c_ have already been injected below it
    a_
  }

  private val program: ZIO[MainEnv, Throwable, Unit] =
    for {
      _ <- ServiceA.runMe()
    } yield ()

  override def run(args: List[String]): URIO[zio.ZEnv, ExitCode] =
    program.provideLayer(layers).exitCode
}
