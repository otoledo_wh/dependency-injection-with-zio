import zio._

trait ServiceCInterface {
  def runMe(): Task[Unit]
}

object ServiceC {

  def runMe(): RIO[Has[ServiceCInterface], Unit] =
    ZIO.serviceWith(_.runMe())

  val live: ZLayer[Any, Nothing, Has[ServiceCInterface]] =
    ZLayer.succeed(ServiceCLive())
}

case class ServiceCLive() extends ServiceCInterface {
  override def runMe(): Task[Unit] =
    ZIO.effect(println("Yo!! ServiceC has run!"))
}
