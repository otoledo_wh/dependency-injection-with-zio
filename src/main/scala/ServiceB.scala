import zio._

trait ServiceBInterface {
  def runMe(): Task[Unit]
}

object ServiceB {

  def runMe(): RIO[Has[ServiceBInterface], Unit] =
    ZIO.serviceWith(_.runMe())

  val live: ZLayer[Has[ServiceCInterface], Nothing, Has[ServiceBInterface]] =
    ZLayer.fromService(ServiceBLive(_))
}

case class ServiceBLive(serviceC: ServiceCInterface) extends ServiceBInterface {
  override def runMe(): Task[Unit] =
    ZIO.effect(println("Hello! ServiceB has run!")) *> serviceC.runMe()
}
